package websocketsapp;

public class Ficha {

	String numero;
	String color;
	boolean selected;
	boolean agarrada;
	double top,left;
	

    public double getTop() {
		return top;
	}

	public void setTop(double top) {
		this.top = top;
	}

	public double getLeft() {
		return left;
	}

	public void setLeft(double left) {
		this.left = left;
	}

	/**
     * @return the selected
     */
    public boolean isSelected() {
        return selected;
    }

    /**
     * @param selected the selected to set
     */
    public void setSelected(boolean selected) {
        this.selected = selected;
    }

  

    /**
     * @return the color
     */
    public String getColor() {
        return color;
    }

    /**
     * @param color the color to set
     */
    public void setColor(String color) {
        this.color = color;
    }

    /**
     * @return the numero
     */
    public String getNumero() {
        return numero;
    }

    /**
     * @param numero the numero to set
     */
    public void setNumero(String numero) {
        this.numero = numero;
    }

    /**
     * @return the agarrada
     */
    public boolean isAgarrada() {
        return agarrada;
    }

    /**
     * @param agarrada the agarrada to set
     */
    public void setAgarrada(boolean agarrada) {
        this.agarrada = agarrada;
    }


}
