package websocketsapp;

import java.net.InetSocketAddress;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;

import org.java_websocket.WebSocket;
import org.java_websocket.handshake.ClientHandshake;
import org.java_websocket.server.WebSocketServer;
import org.json.JSONException;
import org.json.JSONObject;
import Listas.*;


public class Server extends WebSocketServer{
	public static  WebSocket co;
//	private static Map<Integer,Set<WebSocket>> Rooms = new HashMap<>();
	private static List<WebSocket> clients=new ArrayList<WebSocket>();
	Controlador controlador;
	public int numeroJugadores=0;

    public Server() {
        super(new InetSocketAddress(30001));
        //this.controlador= controlador;
    }

    @Override
    public void onOpen(WebSocket conn, ClientHandshake handshake) {
    	numeroJugadores++;
        System.out.println("New client connected: " + conn.getRemoteSocketAddress() + " hash " + conn.getRemoteSocketAddress().hashCode());
        clients.add(conn);
        co=conn;
        //sendToAll(conn,g,numeroJugadores-1);
        if(numeroJugadores==1) {
        	sendSpecify(conn,"PUERCOESPIN",numeroJugadores-1);
        }
        if(numeroJugadores==2) {
        	sendSpecify(conn,"LEON",numeroJugadores-1);
        }
        if(numeroJugadores==3) {
        	sendSpecify(conn,"TIGRE",numeroJugadores-1);
        }
        if(numeroJugadores==4) {
        	sendSpecify(conn,"MAPACHE",numeroJugadores-1);
        }
        
        
    }
    public void sendSpecify(WebSocket conn, String message,int numero) {
 		WebSocket c = (WebSocket)clients.get(numero);
        if (c == conn) c.send(message);
 }
    @Override
    public void onMessage(WebSocket conn, String message) {
    	 Translate ts= new Translate();
    	
        JSONObject obj = new JSONObject(message);
      String msgtype = obj.getString("type");
     
      System.out.println(msgtype);
  	if(msgtype.equals("GanadorPUERCOESPIN")) {
  		System.out.println("ENTROOOOOOOOOOOOOOOOOOOOOOOOOOO");
    		ts.ganadorPUERCOESPIN();
    	}
        if(msgtype.equals("GanadorLEON")) {
      		ts.ganadorLEON();
      	}
        if(msgtype.equals("GanadorTIGRE")) {
      		ts.ganadorTIGRE();
      	}
        if(msgtype.equals("GanadorMAPACHE")) {
      		ts.ganadorMAPACHE();
      	}
      if(msgtype.equals("lista")) {
    	  ts.pasarTablero(conn, message);
      }
      
      if(msgtype.equals("arraySobrantes")) {
    	 
    	  ts.pasarSobrantes(message);
      }
      if(msgtype.equals("arrayFichasPUERCOESPIN")) {
    	 
    	  ts.pasarFichasJugador1(message);
    	 // ts.pasarTablero(message);
      }
      if(msgtype.equals("arrayFichasTIGRE")) {
    	 
    	  ts.pasarFichasJugador3(message);
    	 // ts.pasarTablero(message);
      }
      if(msgtype.equals("arrayFichasLEON")) {
    	  
    	  ts.pasarFichasJugador2(message);
    	 // ts.pasarTablero(message);
      }
      if(msgtype.equals("arrayFichasMAPACHE")) {
    	  
    	  ts.pasarFichasJugador4(message);
    	 // ts.pasarTablero(message);
      }

//        Set<WebSocket> s;
//        try {
//            String msgtype = obj.getString("type");
//            switch (msgtype) {
//                case "GETROOM":
//                    myroom = generateRoomNumber();
//                    s = new HashSet<>();
//                    s.add(conn);
//                    Rooms.put(myroom, s);
//                    System.out.println("Generated new room: " + myroom);
//                    conn.send("{\"type\":\"GETROOM\",\"value\":" + myroom + "}");
//                    break;
//                case "ENTERROOM":
//                    myroom = obj.getInt("value");
//                    System.out.println("New client entered room " + myroom);
//                    s = Rooms.get(myroom);
//                    s.add(conn);
//                    Rooms.put(myroom, s);
//                    break;
//                default:
//                    sendToAll(conn, message);
//                    break;
//            }
//        } catch (JSONException e) {
//            sendToAll(conn, message);
//        }
    	/*conn.send("listo! recibido!");
    	sendToAll(conn,message,0);
    	co=conn;*/
    	
    	//controlador.intermediario(message);
    }

    @Override
    public void onClose(WebSocket conn, int code, String reason, boolean remote) {
        System.out.println("Client disconnected: " + reason);
    }

    @Override
    public void onError(WebSocket conn, Exception exc) {
        System.out.println("Error happened: " + exc);
    }

    private int generateRoomNumber() {
        return new Random(System.currentTimeMillis()).nextInt();
    }

    public static void sendToAll(String message,int numero) {
       /*
    	Iterator it = Rooms.get(myroom).iterator();
        while (it.hasNext()) {
            WebSocket c = (WebSocket)it.next();
            if (c != conn) c.send(message);
        }
        */
    
    
    		WebSocket c = (WebSocket)clients.get(numero);
             c.send(message);
    	
    }


}
