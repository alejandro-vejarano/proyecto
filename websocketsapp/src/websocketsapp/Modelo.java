package websocketsapp;

import java.util.Random;

public class Modelo {

	 public int contador = 0;
	    public int col;
	    public int num;
	    public int contadorcar2;
	public static Ficha[] FichasJugador1;
	public static Ficha[] FichasJugador2;
	public static Ficha[] FichasJugador3;
	public static Ficha[] FichasJugador4;
	public int contadorsobra;
	public static Ficha[] tablero;
	public Ficha[][] baraja;
	public static Ficha[] sobrantes;
	
    public void iniciar() {

        this.FichasJugador1 = new Ficha[51];
        this.FichasJugador2 = new Ficha[51];
        this.FichasJugador3 = new Ficha[51];
        this.FichasJugador4 = new Ficha[51];
        this.baraja = new Ficha[5][26];
        this.tablero = new Ficha[500];
        this.sobrantes = new Ficha[38];
        
        for (int i = 0; i < 5; i++) {
            for (int j = 0; j < 26; j++) {

                baraja[i][j] = new Ficha();

                if (i == 0) {
                    baraja[i][j].setNumero(String.valueOf(contador + 1));
                    baraja[i][j].setColor("Amarillo");
                    contador++;

                }
                if (i == 1) {
                    baraja[i][j].setNumero(String.valueOf(contador + 1));
                    baraja[i][j].setColor("Azul");
                    contador++;
                }
                if (i == 2) {
                    baraja[i][j].setNumero(String.valueOf(contador + 1));
                    baraja[i][j].setColor("Rojo");
                    contador++;
                }
                if (i == 3) {
                    baraja[i][j].setNumero(String.valueOf(contador + 1));
                    baraja[i][j].setColor("Negro");
                    contador++;
                }

                if (contador > 12) {

                    contador = 0;
                }

            }
            
            baraja[4][0] = new Ficha();
            baraja[4][0].setNumero("14");
            baraja[4][0].setColor("Rojo");
            
            baraja[4][1] = new Ficha();
            baraja[4][1].setNumero("14");
            baraja[4][1].setColor("Negro");
         
        }
        
        for (int i = 0; i < 51; i++) {
        	FichasJugador1[i] = new Ficha();
        	FichasJugador2[i] = new Ficha();
        	FichasJugador3[i] = new Ficha();
        	FichasJugador4[i] = new Ficha();
        }
    	
        for (int i = 0; i < 38; i++) {
            sobrantes[i] = new Ficha();

        }
        for (int i = 0; i < 500; i++) {
        	
                tablero[i] = new Ficha();
                tablero[i].setSelected(false);

         
        }
        

        repartir();
    }
    
    public void repartir() {

        for (int i = 14; i < 51; i++) {
        	FichasJugador1[i].setSelected(true);
        	FichasJugador2[i].setSelected(true);
        	FichasJugador3[i].setSelected(true);
        	FichasJugador4[i].setSelected(true);
        }
        for (int i = 0; contadorcar2 < 14; i++) {

            Random r = new Random();
            int col = r.nextInt(5);
            int num = r.nextInt(26);
            if (col == 0 && !baraja[0][num].isAgarrada()) {
            	FichasJugador1[contadorcar2].setColor("Amarillo");
            	FichasJugador1[contadorcar2].setNumero(baraja[0][num].getNumero());
                baraja[0][num].setAgarrada(true);

               
                contadorcar2++;
            }
            if (col == 1 && !baraja[1][num].isAgarrada()) {
            	FichasJugador1[contadorcar2].setColor("Azul");
            	FichasJugador1[contadorcar2].setNumero(baraja[1][num].getNumero());
                baraja[1][num].setAgarrada(true);

                
                contadorcar2++;
            }
            if (col == 2 && !baraja[2][num].isAgarrada()) {
            	FichasJugador1[contadorcar2].setColor("Rojo");
            	FichasJugador1[contadorcar2].setNumero(baraja[2][num].getNumero());
                baraja[2][num].setAgarrada(true);

                contadorcar2++;
            }
            if (col == 3 && !baraja[3][num].isAgarrada()) {
            	FichasJugador1[contadorcar2].setColor("Negro");
            	FichasJugador1[contadorcar2].setNumero(baraja[3][num].getNumero());
                baraja[3][num].setAgarrada(true);

                contadorcar2++;
            }
            

        }
        /*System.out.println("Fichas ");
        for (int i = 0; i < 5; i++) {
        	for	(int j = 0; j < 26; j++){
        		System.out.print(baraja[i][j].getColor());
                System.out.print(baraja[i][j].getNumero());
                
        	}
        	
        
        }
        */
        //System.out.println(carmaquina[i].getColor());
        //System.out.println(carmaquina[i].getNumero());
        //}
        //ponercartacentro();
        contadorcar2=0;
        for (int i = 0; contadorcar2 < 14; i++) {

            Random r = new Random();
            int col = r.nextInt(5);
            int num = r.nextInt(26);
            if (col == 0 && !baraja[0][num].isAgarrada()) {
            	FichasJugador2[contadorcar2].setColor("Amarillo");
            	FichasJugador2[contadorcar2].setNumero(baraja[0][num].getNumero());
                baraja[0][num].setAgarrada(true);

                
                contadorcar2++;
            }
            if (col == 1 && !baraja[1][num].isAgarrada()) {
            	FichasJugador2[contadorcar2].setColor("Azul");
            	FichasJugador2[contadorcar2].setNumero(baraja[1][num].getNumero());
                baraja[1][num].setAgarrada(true);

               
                contadorcar2++;
            }
            if (col == 2 && !baraja[2][num].isAgarrada()) {
            	FichasJugador2[contadorcar2].setColor("Rojo");
            	FichasJugador2[contadorcar2].setNumero(baraja[2][num].getNumero());
                baraja[2][num].setAgarrada(true);

               
                contadorcar2++;
            }
            if (col == 3 && !baraja[3][num].isAgarrada()) {
            	FichasJugador2[contadorcar2].setColor("Negro");
            	FichasJugador2[contadorcar2].setNumero(baraja[3][num].getNumero());
                baraja[3][num].setAgarrada(true);

               
                contadorcar2++;
            }

        }
        /*
        System.out.println(" ");
        System.out.println("Fichas 1 ");
        for (int i = 0; i < 14; i++) {
        	
        System.out.print(FichasJugador1[i].getColor());
        System.out.print(FichasJugador1[i].getNumero());
        
        }
        System.out.println(" ");
        System.out.println("Fichas 2 ");
        for (int i = 0; i < 14; i++) {
        	
        System.out.print(FichasJugador2[i].getColor());
        System.out.print(FichasJugador2[i].getNumero());
        
        }
        */
        //System.out.println(carmaquina[i].getColor());
        //System.out.println(carmaquina[i].getNumero());
        //}
        //ponercartacentro();
        contadorcar2=0;
        for (int i = 0; contadorcar2 < 14; i++) {
        	
            Random r = new Random();
            int col = r.nextInt(5);
            int num = r.nextInt(26);
            if (col == 0 && !baraja[0][num].isAgarrada()) {
            	FichasJugador3[contadorcar2].setColor("Amarillo");
            	FichasJugador3[contadorcar2].setNumero(baraja[0][num].getNumero());
                baraja[0][num].setAgarrada(true);

                
                contadorcar2++;
            }
            if (col == 1 && !baraja[1][num].isAgarrada()) {
            	FichasJugador3[contadorcar2].setColor("Azul");
            	FichasJugador3[contadorcar2].setNumero(baraja[1][num].getNumero());
                baraja[1][num].setAgarrada(true);

                
                contadorcar2++;
            }
            if (col == 2 && !baraja[2][num].isAgarrada()) {
            	FichasJugador3[contadorcar2].setColor("Rojo");
            	FichasJugador3[contadorcar2].setNumero(baraja[2][num].getNumero());
                baraja[2][num].setAgarrada(true);

               
                contadorcar2++;
            }
            if (col == 3 && !baraja[3][num].isAgarrada()) {
            	FichasJugador3[contadorcar2].setColor("Negro");
            	FichasJugador3[contadorcar2].setNumero(baraja[3][num].getNumero());
                baraja[3][num].setAgarrada(true);

                
                contadorcar2++;
            }

        }
        /*System.out.println(" ");
        System.out.println("Fichas 3 ");
        for (int i = 0; i < 14; i++) {
        	
        System.out.print(FichasJugador3[i].getColor());
        System.out.print(FichasJugador3[i].getNumero());
        
        }
        */
        //System.out.println(carmaquina[i].getColor());
        //System.out.println(carmaquina[i].getNumero());
        //}
        //ponercartacentro();
        contadorcar2=0;
        for (int i = 0; contadorcar2 < 14; i++) {

            Random r = new Random();
            int col = r.nextInt(5);
            int num = r.nextInt(26);
            if (col == 0 && !baraja[0][num].isAgarrada()) {
            	FichasJugador4[contadorcar2].setColor("Amarillo");
            	FichasJugador4[contadorcar2].setNumero(baraja[0][num].getNumero());
                baraja[0][num].setAgarrada(true);

               
                contadorcar2++;
            }
            if (col == 1 && !baraja[1][num].isAgarrada()) {
            	FichasJugador4[contadorcar2].setColor("Azul");
            	FichasJugador4[contadorcar2].setNumero(baraja[1][num].getNumero());
                baraja[1][num].setAgarrada(true);

                contadorcar2++;
            }
            if (col == 2 && !baraja[2][num].isAgarrada()) {
            	FichasJugador4[contadorcar2].setColor("Rojo");
            	FichasJugador4[contadorcar2].setNumero(baraja[2][num].getNumero());
                baraja[2][num].setAgarrada(true);

                
                contadorcar2++;
            }
            if (col == 3 && !baraja[3][num].isAgarrada()) {
            	FichasJugador4[contadorcar2].setColor("Negro");
            	FichasJugador4[contadorcar2].setNumero(baraja[3][num].getNumero());
                baraja[3][num].setAgarrada(true);

               
                contadorcar2++;
            }

        }
        
        /*System.out.println(" ");
        System.out.println("Fichas 4 ");
        for (int i = 0; i < 14; i++) {
        	
        System.out.print(FichasJugador4[i].getColor());
        System.out.print(FichasJugador4[i].getNumero());
        
        }
        */
     
       
       
     llenarbaraja();
        
    }
    public void llenarbaraja() {
        for (int i = 0; i < 20; i++) {
            for (int j = 0; j < 4; j++) {
                if (!baraja[j][i].isAgarrada()) {
                    sobrantes[contadorsobra].setNumero(baraja[j][i].getNumero());
                    sobrantes[contadorsobra].setColor(baraja[j][i].getColor());
                    sobrantes[contadorsobra].setSelected(true);
                    if (contadorsobra < 37) {
                        contadorsobra++;
                    }

                }
            }
        }
        /*
        sobrantes[37].setNumero("14");
        sobrantes[37].setColor("Rojo");
        sobrantes[37].setSelected(true);
        
        sobrantes[38].setNumero("14");
        sobrantes[38].setColor("Negro");
        sobrantes[38].setSelected(true);
         */
    }
    
}
