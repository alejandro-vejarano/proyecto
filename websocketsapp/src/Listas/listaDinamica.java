/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Listas;
import websocketsapp.*;
/**
 *
 * @author Alejandro
 */
public class listaDinamica {

    private Nodo primero;
    private Nodo ultimo;

    public listaDinamica() {
        ultimo = null;
        primero = null;
    }

    public Nodo getPrimero() {
        return primero;
    }

    public Nodo getUltimo() {
        return ultimo;
    }

    public void setPrimero(Nodo primero) {
        this.primero = primero;
    }

    public void setUltimo(Nodo ultimo) {
        this.ultimo = ultimo;
    }
    
    public int contador() {
        int aux=1;
    	try {
        Nodo actual = primero;
            while (actual.getSig() != null) {
                actual = actual.getSig();
                aux++;
            }
    	}catch(Exception e) {
    	}
        return aux;
    }
    
    public void eliminar(){
        primero = null;
        ultimo=null;
    }
    

    public void insertar(int numero, String color, double left, double top) {
        Nodo nuevo = new Nodo(numero, color, left, top);

        if (primero == null) {
        	nuevo.setSelected(true);
            primero = nuevo;
            ultimo = nuevo;
        } else {
            Nodo actual = primero;

            while (actual.getSig() != null) {
            	nuevo.setSelected(true);
                actual = actual.getSig();
            }
        	nuevo.setSelected(true);
            actual.setSig(nuevo);
            actual = ultimo;
        }

    }

    public void listar() {
    	try {
        Nodo actual = primero;
            while (actual.getSig() != null) {
                System.out.println(actual.getNumero() + "--" + actual.getColor() + "--" +actual.getLeft()+"--"+actual.getTop());
                actual = actual.getSig();
            }
            System.out.println(actual.getNumero() + "--" + actual.getColor() + "--" +actual.getLeft()+"--"+actual.getTop());
    	}catch(Exception e) {
    		System.out.println("No hay nada");
    	}
        
    }
    
    public void comprobarLinea() {
    	Nodo actual = primero;

        while (actual.getSig() != null) {
        	double dif= actual.getLeft() -actual.getSig().getLeft();
    		boolean rango1=	(dif>=70 && dif<=72);
    		boolean rango2=(dif<=(-70) && dif>=(-72));
    	//	System.out.println(""+dif);
        	if((rango1 || rango2) && actual.getTop() == actual.getSig().getTop()) {
        		System.out.println("Same line caray");
        	}
        	
            actual = actual.getSig();
        }
    }

    public boolean comprobarEscalera() {
        Nodo actual = primero;
        int aux = 0;
        boolean valido = true;
        while (actual.getSig() != null && valido) {
            aux = actual.getNumero() + 1;
            if (actual.getSig().getNumero() != aux) {
                valido = false;
            }
            if (!actual.getSig().getColor().equals(actual.getColor())) {
                valido = false;
            }
            actual = actual.getSig();
        }
        return valido;
    }

    public boolean comprobarIguales() {
        Nodo actual = primero;
        int rojo = 0, azul = 0, amarillo = 0, negro = 0;
        boolean valido = true;

        while (actual.getSig() != null && valido) {
            switch (actual.getColor()) {
                case "rojo":
                    rojo++;
                    break;
                case "azul":
                    azul++;
                    break;
                case "amarillo":
                    amarillo++;
                    break;
                case "negro":
                    negro++;
                    break;
                default:
                    break;
            }
            if (actual.getSig().getColor().equals(actual.getColor())) {
                valido = false;
            }
            if (actual.getNumero() != actual.getSig().getNumero()) {
                valido = false;
            }
            actual = actual.getSig();
        }
        if ((rojo > 1) || (azul > 1) || (amarillo > 1) || (negro > 1)) {
            valido = false;
        }
        return valido;
    }

    public int inicio() {
        Nodo actual = primero;
        int total = 0;
        while (actual.getSig() != null) {
            total += actual.getNumero();
            actual = actual.getSig();
        }
        total += actual.getNumero();
        return total;
    }

    public Nodo separar(int num, String colo, double left, double top) {
        Nodo actual = primero;
        boolean mitad = false;
        Nodo nuevo = new Nodo(num, colo,left,top);
        listaDinamica p = new listaDinamica();

        while (actual.getSig() != null && !mitad) {
            if (actual.getColor().equals(colo) && actual.getNumero() == num) {
                System.out.println("Hey");
                mitad = true;
            } else {
                actual = actual.getSig();
            }
        }

        return actual;
    }

    public listaDinamica separar1(Nodo separa) {
        Nodo actual = primero;
        listaDinamica n1 = new listaDinamica();
            while (actual != separa) {
                n1.insertar(actual.getNumero(), actual.getColor(),actual.getLeft(),actual.getTop());
                actual = actual.getSig();
            }
        
        return n1;
    }

    public listaDinamica separar2(Nodo separa, Nodo aux) {
        Nodo actual = separa;
        listaDinamica n2 = new listaDinamica();

            while (actual.getSig() != null) {
                n2.insertar(actual.getNumero(), actual.getColor(),actual.getLeft(),actual.getTop());
                actual = actual.getSig();
            }
            n2.insertar(actual.getNumero(), actual.getColor(),actual.getLeft(),actual.getTop());
        
        return n2;
    }
    
    public Ficha[] convertirFicha() {
		Nodo actual=primero;
		int i=0,j=0;
		 while (actual.getSig() != null) {
			 actual = actual.getSig();
			 i++;
         }
		 actual=primero;
			Ficha[] f= new Ficha[50];
			System.out.println(""+i+""+j);
			
		 while (actual.getSig() != null && i!=j) {
			 f[j].setColor(actual.getColor());
			 f[j].setNumero(String.valueOf(actual.getNumero()));
			 actual = actual.getSig();
			 j++;
         }

		
    	return f;	
    }

}
