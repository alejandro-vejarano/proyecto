/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Listas;

/**
 *
 * @author Alejandro
 */
public class Nodo {

    private int numero;
    private String color;
    private Nodo siguiente;
    private double left, top;
	private boolean selected;
	private boolean agarrada;

    public Nodo(int elem1, String elem2, double le, double to) {
        numero = elem1;
        color = elem2;
        left=le;
        top=to;
    }

    public double getLeft() {
		return left;
	}

	public void setLeft(double left) {
		this.left = left;
	}

	public double getTop() {
		return top;
	}

	public void setTop(double top) {
		this.top = top;
	}

	public int getNumero() {
        return numero;
    }

    public String getColor() {
        return color;
    }

    public Nodo getSig() {
        return siguiente;
    }

    public void setNumero(int elem1) {
        numero = elem1;
    }

    public void setColor(String elem2) {
        color = elem2;
    }

    public void setSig(Nodo sig) {
        siguiente = sig;
    }

	public Nodo getSiguiente() {
		return siguiente;
	}

	public void setSiguiente(Nodo siguiente) {
		this.siguiente = siguiente;
	}

	public boolean isSelected() {
		return selected;
	}

	public void setSelected(boolean selected) {
		this.selected = selected;
	}

	public boolean isAgarrada() {
		return agarrada;
	}

	public void setAgarrada(boolean agarrada) {
		this.agarrada = agarrada;
	}


  
}
